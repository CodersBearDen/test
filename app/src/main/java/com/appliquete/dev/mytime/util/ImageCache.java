package com.appliquete.dev.mytime.util;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.LruCache;

/**
 * Created by mac on 5/29/17.
 */

public class ImageCache {
    private static final String TAG = "ImageCache";
    private LruCache<String, Bitmap> memoryCache;

    private ImageCache(float memChacheSizePercent) { init(memChacheSizePercent); }

    public static ImageCache getInstance(
            FragmentManager fragmentManager, float memCacheSizePercent) {

        final RetainFragment retainFragment = findOrCreateRetainFragment(fragmentManager);

        ImageCache imageCache = (ImageCache) retainFragment.getObject();

        if (imageCache == null) {
            imageCache = new ImageCache(memCacheSizePercent);
            retainFragment.setObject(imageCache);
        }

        return imageCache;
    }

    private void init(float memCacheSizePercent) {
        int memCacheSize = calculateMemCacheSize(memCacheSizePercent);

        memoryCache = new LruCache<String, Bitmap>(memCacheSize) {
          @Override
            protected int sizeOf(String key, Bitmap bitmap) {
              final int bitmapSize = getBitmapSize(bitmap) / 1024;
              return bitmapSize == 0 ? 1 : bitmapSize;
          }
        };
    }

    public void addBitmapToCache(String data, Bitmap bitmap) {
        if(data == null || bitmap == null) {
            return;
        }

        if (memoryCache != null && memoryCache.get(data) == null) {
            memoryCache.put(data, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String data) {
        if(memoryCache != null) {
            final Bitmap memBitmap = memoryCache.get(data);
            if (memBitmap != null) {
//                if (BuildConfig.DEBUG)
                return memBitmap;
            }
        }
        return null;
    }

    @TargetApi(12)
    public static int getBitmapSize(Bitmap bitmap) {
        if(Utils.hasHoneycombMR1()) {
            return bitmap.getByteCount();
        }
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public static int calculateMemCacheSize(float percent) {
        if (percent < 0.05f || percent > 0.8f) {
            throw new IllegalArgumentException("setMemCachePercent - percent musty be "
                    + "between 0.05 and 0.8 (inclusive)");
        }
        return Math.round(percent * Runtime.getRuntime().maxMemory() / 1024);
    }

    public static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
        RetainFragment retainFragment = (RetainFragment) fm.findFragmentByTag(TAG);

        if(retainFragment == null) {
            retainFragment = new RetainFragment();
            fm.beginTransaction().add(retainFragment, TAG).commitAllowingStateLoss();
        }

        return retainFragment;
    }

    public static class RetainFragment extends Fragment {
        private Object rfObject;

        public RetainFragment() {}

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            setRetainInstance(true);
        }

        public void setObject(Object object) {rfObject = object;}

        public Object getObject() {
            return rfObject;
        }
    }
}