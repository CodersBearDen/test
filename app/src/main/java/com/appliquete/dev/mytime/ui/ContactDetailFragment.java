package com.appliquete.dev.mytime.ui;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appliquete.dev.mytime.util.ImageLoader;
import com.appliquete.dev.mytime.util.Utils;

import java.net.URI;

/**
 * Created by mac on 5/30/17.
 */

public class ContactDetailFragment extends Fragment implements
        LoaderManager.LoaderCallbacks<Cursor> {


    public static final String EXTRA_CONTACT_URI =
            "com.example.android.contactslist.ui.EXTRA_CONTACT_URI";

    // Defines a tag for identifying log entries
    private static final String TAG = "ContactDetailFragment";

    // The geo Uri scheme prefix, used with Intent.ACTION_VIEW to form a geographical address
    // intent that will trigger available apps to handle viewing a location (such as Maps)
    private static final String GEO_URI_SCHEME_PREFIX = "geo:0,0?q=";

    // Whether or not this fragment is showing in a two pane layout
    private boolean isTwoPaneLayout;

    private Uri contactUri; // Stores the contact Uri for this fragment instance
    private ImageLoader imageLoader; // Handles loading the contact image in a background thread

    private ImageView imageView;
    private LinearLayout detailsLayout;
    private TextView emptyView;
    private TextView contactName;
    private MenuItem editContactMenuItem;

    public static ContactDetailFragment newInstance(Uri contactUri) {
        final ContactDetailFragment fragment = new ContactDetailFragment();

        final Bundle args = new Bundle();
        args.putParcelable(EXTRA_CONTACT_URI, contactUri);

        fragment.setArguments(args);

        return fragment;
    }

    public ContactDetailFragment() {}

    public void setContact(Uri contactLookupUri) {
        if (Utils.hasHoneycomb()) {
            contactUri = contactLookupUri;
        } else {
            contactUri = ContactsContract.Contacts.lookupContact(getActivity().getContentResolver(), contactLookupUri);
        }

        if (contactLookupUri != null) {
            imageLoader.loadImage(contactUri, imageView);

            imageView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);

            if (editContactMenuItem !=  null) {
                editContactMenuItem.setVisible(true);
            }

            getLoaderManager().restartLoader(ContactDetailQuery.QUERY_ID, null, this);
            getLoaderManager().restartLoader(ContactAddressQuery.QUERY_ID, null, this);
        } else {
            imageView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            detailsLayout.removeAllViews();
            if (contactName != null) {
                contactName.setText("");
            }
            if (editContactMenuItem != null) {
                editContactMenuItem.setVisible(false);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isTwoPaneLayout = getResources().getBoolean(R.bool.has_two_panes);

        setHasOptionsMenu(true);

        imageLoader = new ImageLoader(getActivity(), getLargestScreenDimension()) {
            @Override
            protected Bitmap processBitmap(Object data) {
                return loadContactPhoto((Uri)data, getImageSize());
            }
        };

//        imageLoader.setLoadingImage(android.R.drawable.i);
        imageLoader.setImageFadeIn(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
//        final View detailView =
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
